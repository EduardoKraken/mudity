import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		datosUsuario:'',
	},

	mutations:{
		DATOS_USUARIO(state, datosUsuario){
      state.datosUsuario = datosUsuario
		},
	},

	actions:{
		guardarInfo({commit, dispatch}, usuario){
			commit('DATOS_USUARIO',usuario)
		},

	},

	getters:{
		getdatosUsuario(state){
			return state.datosUsuario
		},

	}
}