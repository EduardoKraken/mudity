import Vue from 'vue'
import VueRouter from 'vue-router'


import Login          from '@/views/usuario/Login.vue'
import Home           from '@/views/Home.vue'

import Usuarios       from '@/views/catalogos/Usuarios.vue'
import Articulos      from '@/views/catalogos/Articulos.vue'
import Unidades       from '@/views/catalogos/Unidades.vue'
import Puestos        from '@/views/catalogos/Puestos.vue'

import Entradas       from '@/views/almacen/Entradas.vue'
import Salidas        from '@/views/almacen/Salidas.vue'
import Almacen        from '@/views/almacen/Almacen.vue'
import Discrepancias  from '@/views/almacen/Discrepancias.vue'


import Proyectos                from '@/views/produccion/Proyectos.vue'
import SeccionesProyectos       from '@/views/proyectos/SeccionesProyectos.vue'
import SeccionProyectoUsuario   from '@/views/proyectos/SeccionProyectoUsuario.vue'
import Etapas                   from '@/views/produccion/Etapas.vue'
import CentroCalidad            from '@/views/produccion/CentroCalidad.vue'
import Progresos                from '@/views/produccion/Progresos.vue'
import Clientes                 from '@/views/catalogos/Clientes.vue'


// CRM PROSPECTOS
// import Etapas                       from '@/views/crm-prospectos/catalogos/Etapas.vue'
import MedioContacto                from '@/views/crm-prospectos/catalogos/MedioContacto.vue'
import Anuncios                     from '@/views/crm-prospectos/catalogos/Anuncios.vue'
import Tareas                       from '@/views/crm-prospectos/catalogos/Tareas.vue'
import Campanias                    from '@/views/crm-prospectos/catalogos/Campanias.vue'
import Motivos                      from '@/views/crm-prospectos/catalogos/Motivos.vue'
import Fuentes                      from '@/views/crm-prospectos/catalogos/Fuentes.vue'
import DetalleFuentes               from '@/views/crm-prospectos/catalogos/DetalleFuentes.vue'


import Prospectos                   from '@/views/crm-prospectos/prospectos/Prospectos.vue'
import VerProspecto                 from '@/views/crm-prospectos/prospectos/VerProspecto.vue'

import store      from '@/store'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: '',
  base: process.env.BASE_URL,
  routes: [

    { path: '/'          , name: 'Login'                , component: Login          , meta: { libre: true }},
    { path: '/home'      , name: 'Home'                 , component: Home           , meta: { libre: true }},
    { path: '/usuarios'  , name: 'Usuarios'             , component: Usuarios       , meta: { libre: true }},

    { path: '/articulos'  , name: 'Articulos'           , component: Articulos      , meta: { libre: true }},
    { path: '/unidades'   , name: 'Unidades'            , component: Unidades       , meta: { libre: true }},
    { path: '/puestos'    , name: 'Puestos'             , component: Puestos        , meta: { libre: true }},

    { path: '/entradas'       , name: 'Entradas'        , component: Entradas       , meta: { libre: true }},
    { path: '/salidas'        , name: 'Salidas'         , component: Salidas        , meta: { libre: true }},
    { path: '/almacen'        , name: 'Almacen'         , component: Almacen        , meta: { libre: true }},
    { path: '/discrepancias'  , name: 'Discrepancias'   , component: Discrepancias  , meta: { libre: true }},

    { path: '/proyectos'      , name: 'Proyectos'       , component: Proyectos      , meta: { libre: true }},
    { path: '/etapas'         , name: 'Etapas'          , component: Etapas         , meta: { libre: true }},
    { path: '/centrocalidad'  , name: 'CentroCalidad'   , component: CentroCalidad  , meta: { libre: true }},
    { path: '/progresos'      , name: 'Progresos'       , component: Progresos      , meta: { libre: true }},
    { path: '/clientes'       , name: 'Clientes'        , component: Clientes       , meta: { libre: true }},

    // CRM PROSPECTOS
    { path: '/mediocontacto'  , name: 'MedioContacto'   , component: MedioContacto  , meta: { libre: true}},
    { path: '/anuncios'       , name: 'Anuncios'        , component: Anuncios       , meta: { libre: true}},
    { path: '/tareas'         , name: 'Tareas'          , component: Tareas         , meta: { libre: true}},
    { path: '/motivos'        , name: 'Motivos'         , component: Motivos        , meta: { libre: true }},
    { path: '/prospectos'     , name: 'Prospectos'      , component: Prospectos     , meta: { libre: true}},
    { path: '/verprospecto'   , name: 'VerProspecto'    , component: VerProspecto   , meta: { libre: true}},
    { path: '/fuentes'        , name: 'Fuentes'         , component: Fuentes        , meta: { libre: true}},
    { path: '/detallefuentes' , name: 'DetalleFuentes'  , component: DetalleFuentes , meta: { libre: true}},
    { path: '/campanias'      , name: 'Campanias'       , component: Campanias      , meta: { libre: true}},

    { path: '/seccionesproyectos'      , name: 'SeccionesProyectos'        , component: SeccionesProyectos      , meta: { libre: true}},
    { path: '/seccionproyectousuario'  , name: 'SeccionProyectoUsuario'    , component: SeccionProyectoUsuario  , meta: { libre: true}},
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.libre)) {
    next()
  } else if (store.state.login.getdatosUsuarioMudity) {
    if (to.matched.some(record => record.meta.USUARIO)) {
      next()
    }
  } else {
    next({
      name: 'Login'
    })
  }
})

export default router